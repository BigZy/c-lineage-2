﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lineage2.Startup))]
namespace Lineage2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
